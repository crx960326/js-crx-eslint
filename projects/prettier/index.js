const { name, version } = require('./package.json');

module.exports = {
   configs: {
      recommended: require('./recommended.js'),
   },
   meta: {
      name,
      version,
   },
   processors: {},
   rules: {},
};
