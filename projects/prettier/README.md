# @crx-eslint/eslint-plugin-prettier

#### 介绍
eslint+prettier配置包，已包含插件：[eslint-config-prettier](https://www.npmjs.com/package/eslint-config-prettier)、[eslint-plugin-prettier](https://www.npmjs.com/package/eslint-plugin-prettier)。

#### 安装教程

```shell
npm i -D @crx-eslint/eslint-plugin-prettier
```

#### 使用说明

```json
{
   "extends": ["plugin:@crx-eslint/eslint-plugin-prettier/recommended"]
}
```


