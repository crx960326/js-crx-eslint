const { name, version } = require('./package.json');

module.exports = {
  configs: {
    standard: require('@crx-eslint/eslint-plugin-standard/recommended'),
    prettier: require('@crx-eslint/eslint-plugin-prettier/recommended'),
    typescript: require('@crx-eslint/eslint-plugin-typescript/recommended'),
    vue2: require('@crx-eslint/eslint-plugin-vue/vue2'),
    vue3: require('@crx-eslint/eslint-plugin-vue/vue3'),
  },
  meta: {
    name,
    version
  },
  processors: {},
  rules: {}
};
