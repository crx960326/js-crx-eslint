# @crx-eslint/eslint-plugin prettier

#### 介绍
eslint配置包。

#### 安装教程

```shell
npm i -D eslint @crx-eslint/eslint-plugin prettier
```

#### 使用说明

1. 标准规则

```json
{
   "extends": ["plugin:@crx-eslint/standard"]
}
```

2. typescript规则

```json
{
   "extends": ["plugin:@crx-eslint/typescript"]
}
```

3. prettier规则

```json
{
   "extends": ["plugin:@crx-eslint/prettier"]
}
```

4. vue规则
```json
{
   // plugin:@crx-eslint/vue2
   "extends": ["plugin:@crx-eslint/vue3"]
}
```


