# @crx-eslint/eslint-plugin-standard

#### 介绍
eslint标准配置包，已包含插件：[eslint-plugin-import](https://www.npmjs.com/package/eslint-plugin-import)、[eslint-plugin-sort-keys-fix](https://www.npmjs.com/package/eslint-plugin-sort-keys-fix)。

#### 安装教程

```shell
npm i -D @crx-eslint/eslint-plugin-standard
```

#### 使用说明

```json
{
   "extends": ["plugin:@crx-eslint/eslint-plugin-standard/recommended"]
}
```


