# @crx-eslint/eslint-plugin-typescript

#### 介绍
eslint的typescript配置包，已包含插件：[eslint-plugin-unused-imports](https://www.npmjs.com/package/eslint-plugin-unused-imports)、[@typescript-eslint/eslint-plugin](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin)、[@typescript-eslint/parser](https://www.npmjs.com/package/@typescript-eslint/parser)。

#### 安装教程

```shell
npm i -D @crx-eslint/eslint-plugin-typescript
```

#### 使用说明

```json
{
   "extends": ["plugin:@crx-eslint/eslint-plugin-typescript/recommended"]
}
```


