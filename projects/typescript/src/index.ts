const { name: pluginName, version } = require('./package.json');

module.exports = {
   configs: {
      recommended: require('./recommended.js'),
   },
   meta: {
      name: pluginName,
      version,
   },
   processors: {},
   rules: {},
};
