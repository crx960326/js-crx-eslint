# @crx-eslint/eslint-plugin-vue

#### 介绍
eslint的vue配置包，已包含插件：[eslint-plugin-vue](https://www.npmjs.com/package/eslint-plugin-vue)。

#### 安装教程

```shell
npm i -D @crx-eslint/eslint-plugin-vue
```

#### 使用说明

```json
{
   // plugin:@crx-eslint/eslint-plugin-vue/vue2
   "extends": ["plugin:@crx-eslint/eslint-plugin-vue/vue3"]
}
```


