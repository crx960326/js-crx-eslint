const prettierConfig = require("@crx-eslint/eslint-plugin-prettier/prettierrc.json");

module.exports = {
  ...prettierConfig
}