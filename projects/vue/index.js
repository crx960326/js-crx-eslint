const { name, version } = require('./package.json');

module.exports = {
   configs: {
      vue2: require('./vue2.json'),
      vue3: require('./vue3.json'),
   },
   meta: {
      name,
      version,
   },
   processors: {},
   rules: {},
};
